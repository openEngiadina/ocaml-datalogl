(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax

let beverage = Rdf.Namespace.make_namespace "http://rdfs.co/bevon/beverage/"

module Datalog = Datalogl.Make (struct
  type t = Rdf.Term.t

  let compare = Rdf.Term.compare

  let parser =
    let constant iri = Angstrom.(return @@ Rdf.Term.of_iri iri) in
    Angstrom.(
      choice ~failure_msg:"not a valid term"
        [
          string "type" *> (constant @@ Rdf.Namespace.rdf "type");
          string "sp" *> (constant @@ Rdf.Namespace.rdfs "subPropertyOf");
          string "sc" *> (constant @@ Rdf.Namespace.rdfs "subClassOf");
          string "dom" *> (constant @@ Rdf.Namespace.rdfs "domain");
          string "range" *> (constant @@ Rdf.Namespace.rdfs "range");
          string "Tsingtao" *> (constant @@ beverage "Tsingtao");
        ])

  let pp = Rdf.Term.pp
end)

let seq_of_file file =
  let ic = open_in file in
  let rec read () =
    try
      let line = input_char ic in
      Seq.Cons (line, read)
    with
    | End_of_file ->
        close_in ic;
        Seq.Nil
    | e ->
        close_in_noerr ic;
        raise e
  in
  read

let read_ttl file =
  seq_of_file file |> String.of_seq |> Rdf_serd.Turtle.parse_to_graph

let edb graph predicate pattern =
  match predicate with
  | "triples" ->
      Rdf.Graph.to_triples graph
      |> Seq.map (fun triple ->
             Rdf.Triple.
               [
                 Subject.to_term triple.subject;
                 Predicate.to_term triple.predicate;
                 Object.to_term triple.object';
               ])
      |> Lwt_seq.of_seq
      |> Datalog.Database.select pattern
  | _ -> Lwt_seq.empty

let program =
  let rhodf =
    [
      (* rhodf is an extension of rdf. This corresponds to the simple rules in the paper. *)
      "rhodf(?s,?p,?o) :- triples(?s,?p,?o).";
      (* Subproperty (a) *)
      "rhodf(?a, sp, ?c) :- rhodf(?a, sp, ?b), rhodf(?b, sp, ?c).";
      (* Subproperty (b) *)
      "rhodf(?x, ?b, ?y) :- rhodf(?x, ?a, ?y), rhodf(?a, sp, ?b).";
      (* Subclass (a) *)
      "rhodf(?a, sc, ?c) :- rhodf(?a, sc, ?b), rhodf(?b, sc, ?c).";
      (* Subclass (b) *)
      "rhodf(?x, type, ?b) :- rhodf(?a, sc, ?b), rhodf(?x, type, ?a).";
      (* Typing (a) *)
      "rhodf(?x, type, ?b) :- rhodf(?x, ?a, ?y), rhodf(?a, dom, ?b).";
      (* Typing (b) *)
      "rhodf(?y, type, ?b) :- rhodf(?x, ?a, ?y), rhodf(?a, range, ?b).";
      (* Implicit typing (a) *)
      "rhodf(?x, type, ?b) :- rhodf(?x,  ?c, ?y), rhodf(?a, dom, ?b), \
       rhodf(?c, sc, ?a).";
      (* Implicit typing (b) *)
      "rhodf(?y, type, ?b) :- rhodf(?x, ?c, ?y), rhodf(?a, range, ?b), \
       rhodf(?c, sc, ?a) ."
      (* Omitting the reflexiv subClassOf, subPropertyOf rules out of lazyness.*);
    ]
    |> String.concat "\n"
  in
  {datalog|
    rdf(?s,?p,?o) :- triples(?s,?p,?o).
   |datalog} ^ rhodf
  |> Angstrom.parse_string ~consume:Angstrom.Consume.All Datalog.Program.parser
  |> Result.get_ok

let query graph query = Datalog.query ~database:(edb graph) ~program query

let query_string db q =
  let* q =
    q |> Angstrom.parse_string ~consume:Angstrom.Consume.All Datalog.Atom.parser
    |> function
    | Ok query -> return query
    | Error msg -> Lwt.fail_with msg
  in
  query db q

let main =
  Logs.set_reporter (Logs_fmt.reporter ());
  Logs.set_level ~all:true (Some Logs.Debug);

  (* Datalog.set_debug true; *)
  let graph = read_ttl "bevon.ttl" in

  let* tuples = query_string graph "rhodf(Tsingtao,type,?t)" in

  let types =
    tuples |> Datalog.Tuple.Set.to_seq
    |> Seq.filter_map (function
         | [ _; _; type' ] -> Rdf.Term.to_iri type'
         | _ -> None)
  in

  return
  @@ Logs.app (fun m ->
         m "Tsingtao (%a) has types: %a" Rdf.Iri.pp (beverage "Tsingtao")
           Fmt.(seq ~sep:comma Rdf.Iri.pp)
           types)

let () = Lwt_main.run main
