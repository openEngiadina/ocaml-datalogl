# Type inference with ρdf

This is an example usage of Datalog for type inference over RDF data using RDF Schema and ρdf (rhodf).

ρdf as defined in [Simple and Efficient Minimal RDFS (2009)](https://sci-hub.se/10.1016/j.websem.2009.07.003) defines inference rules for figuring out the types of RDF data.

This example infers the types of Tsingtao using the [BEVON: Beverage Ontology](http://rdfs.co/bevon/latest/html):

```
$ dune exec ./rhodf.exe
Tsingtao (http://rdfs.co/bevon/beverage/Tsingtao) has types: http://data.lirmm.fr/ontologies/food#Food,
http://purl.org/goodrelations/v1#ProductOrServiceModel,
http://rdfs.co/bevon/AlcoholicBeverage, http://rdfs.co/bevon/Beer,
http://rdfs.co/bevon/Beverage, http://rdfs.co/bevon/FermentedBeverage,
http://rdfs.co/bevon/Lager, http://rdfs.co/bevon/PaleLager,
http://rdfs.co/bevon/Pilsner,
http://www.productontology.org/id/Alcoholic_beverage,
http://www.productontology.org/id/Beer,
http://www.productontology.org/id/Drink,
http://www.productontology.org/id/Lager,
http://www.productontology.org/id/Pale_lager,
http://www.productontology.org/id/Pilsner,
http://www.w3.org/2002/07/owl#NamedIndividual,
http://www.w3.org/TR/2003/PR-owl-guide-20031209/food#PotableLiquid
```
