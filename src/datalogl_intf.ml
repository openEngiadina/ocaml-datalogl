(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module type Sigs = sig
  type constant
  (** Constant type see {!module-Datalogl.CONSTANT}. *)

  (** {1 Debugging } *)

  val set_debug : bool -> unit

  (** {1 Datalog Expressions} *)

  module Variable : sig
    (** A Datalog variable is a placeholder for arbirtary constants. *)

    type t

    (** {1 Constructors} *)

    val make : string -> t
    val make_fresh : unit -> t

    (** {1 Predicates and Comparison} *)

    val compare : t -> t -> int

    (** {1 IO} *)

    val parser : t Angstrom.t
    (** [parser] reads a single variable. *)

    val pp : t Fmt.t [@@ocaml.toplevel_printer]

    module Set : sig
      include Set.S with type elt = t
    end
  end

  module Term : sig
    (** A Datalog term is either a {!Variable.t} or a {!constant}. *)

    type t
    (** The type of a term. *)

    (** {1 Constructors} *)

    val make_variable : string -> t
    (** [make_variable id] returns a new variable term with identifier [id]. *)

    val make_constant : constant -> t
    (** [make_constant c] returns a new constant term with value [c]. *)

    (** {1 Predicates and Comparison} *)

    val is_variable : t -> bool
    (** [is_variable term] returns true if [term] is a variable. *)

    val is_constant : t -> bool
    (** [is_constant term] returns true if [term] is a constant. *)

    val compare : t -> t -> int
    (** [compare a b] performs a comparisson between terms [a] and [b]. *)

    (** {1 Mapping} *)

    val map : (string -> 'a) -> (constant -> 'a) -> t -> 'a
    (** [map f g term] applies [f] on [term] if it is a variable or [g] if it is a constant. *)

    (** {1 IO} *)

    val parser : t Angstrom.t
    (** [parser] reads a term. *)

    val pp : t Fmt.t [@@ocaml.toplevel_printer]
    (** [pp] term pretty printer. *)
  end

  (* module Substitution : sig
   *   type t
   * 
   *   val empty : t
   *   val bind : Term.t -> Term.t -> t -> t
   *   val deref : t -> Term.t -> Term.t
   *   val unify_term : ?substitution:t -> Term.t -> Term.t -> t option
   *   val pp : t Fmt.t [@@ocaml.toplevel_printer]
   * end *)

  module Atom : sig
    (** A Datalog atom is a predicate symbol with a list of terms of type {!Term.t}. *)

    (** In Prolog syntax the atom 

    [ make "p" [(Term.make_constant 1) (Term.make_variable @@ Variable.make "x")] ] 

    is written as 

    {v p(1, 2, ?x) v}
     *)

    type t
    (** The type of an atom. *)

    type predicate_symbol = string
    (** Type of a predicate symbol. *)

    (** {1 Constructors} *)

    val make : predicate_symbol -> Term.t list -> t
    (** [make predicate terms] returns a new atom with predicate
    symbol [predicate] and terms [terms]. *)

    (** {1 Predicates and Comparison} *)

    val predicate : t -> predicate_symbol
    val terms : t -> Term.t list
    val equal : t -> t -> bool
    val equal_predicate : t -> t -> bool
    val compare : t -> t -> int

    (* val unify : ?substitution:Substitution.t -> t -> t -> Substitution.t option
     * (\** [unify ?substitution a b] returns the substitution that unifies term [a] with [b]. *\)
     * 
     * val equal_alpha : t -> t -> bool *)

    val is_ground : t -> bool
    (** [is_ground atom] returns true if all terms appearing in the atom are constants. *)

    (** {1 IO} *)

    val parser : t Angstrom.t
    val pp : t Fmt.t [@@ocaml.toplevel_printer]
  end

  module Literal : sig
    (** A literal is a positive or negative {!Atom.t}. *)

    (** Currently this library does not support negation. Negative
    literals may not appear in any clauses. *)

    type t
    (** The type of a literal. *)

    (** {1 Constructurs } *)

    val make_positive : Atom.t -> t
    (** [make_positive atom] returns a new positive literal of the atom [atom]. *)

    val make_negative : Atom.t -> t
    (** [make_negative atom] returns a new negative literal of the atom [atom]. *)

    (** {1 Predicates and Comparison} *)

    val predicate : t -> Atom.predicate_symbol
    val terms : t -> Term.t list
    val equal : t -> t -> bool
    val compare : t -> t -> int

    val is_positive : t -> bool
    (** [is_positive literal] returns true if [literal] is positive. *)

    val is_ground : t -> bool
    (** [is_ground literal] returns true if all terms appearing in the literal are constants. *)

    (** {1 IO} *)

    val parser : t Angstrom.t
    val pp : t Fmt.t [@@ocaml.toplevel_printer]

    module Set : Set.S with type elt = t
  end

  module Clause : sig
    (** A clause are expressions that allow us to deduce facts from other facts. *)

    type t
    (** The type of a clause. *)

    (** {1 Constructors} *)

    val make : Atom.t -> Literal.Set.t -> t
    val make_fact : Atom.t -> t

    val head : t -> Atom.t
    (** {1 Predicates and Comparison} *)

    val body : t -> Literal.Set.t
    val equal : t -> t -> bool

    val is_ground_fact : t -> bool
    (** [is_ground_fact clause] returns true if the body of the clause
    is emtpy and the head is ground (see {!Atom.is_ground}). *)

    (** {1 IO} *)

    (* val fresh : t -> t *)
    val parser : t Angstrom.t
    val pp : t Fmt.t [@@ocaml.toplevel_printer]
  end

  (** {1 Datalog Program} *)

  module Program : sig
    (** A Datalog program is a set of clauses ({!Clause.t}). *)

    type t
    (** The type of a Datalog program. *)

    val empty : t
    (** {1 Constructors} *)

    val add : Clause.t -> t -> t
    val add_seq : Clause.t Seq.t -> t -> t

    (** {1 IO} *)

    val parser : t Angstrom.t
    val pp : t Fmt.t [@@ocaml.toplevel_printer]
  end

  (** {1 Database } *)

  module Tuple : sig
    type t = constant list
    (** A tuple is a list of constants. *)

    module Set : sig
      include Set.S with type elt = t

      val pp : t Fmt.t [@@ocaml.toplevel_printer]
    end

    val pp : t Fmt.t [@@ocaml.toplevel_printer]
    (** [pp] tuple pretty printer. *)
  end

  module Database : sig
    type pattern = constant option list
    (** A pattern on tuples. *)

    type t = Atom.predicate_symbol -> pattern -> Tuple.t Lwt_seq.t
    (** Type of database.

       The database is a function that returns a stream of tuples for a 
       given predicate symbol [predicate] and pattern [pattern].

       For elements of [pattern] that are [Some v] the corresponding
       element (at same position) of all tuples must be equal to [v].

       Tuples must all have the same arity as [pattern].*)

    val empty : t
    (** [empty] is a database that does not contain any tuples. This
    may be useful when evaluating a Datalog program without any
    external database. *)

    val select : pattern -> Tuple.t Lwt_seq.t -> Tuple.t Lwt_seq.t
    (** [select pattern tuples] returns a stream of tuples from
    [tuples] that match with [pattern].

    This is useful if the database you are using does not provide an
    internal mechanism for selecting tuples. Note that for performance
    reasons you should always prefer using the database internal
    mechanism for selecting tuples when available. *)
  end

  (** {1 Evaluation} *)

  type query = Atom.t

  val query :
    database:Database.t -> program:Program.t -> query -> Tuple.Set.t Lwt.t

  (** {2 Incremental Evaulation} *)

  (** The state of a Datalog evaluation can be reused for multiple
  queries or for re-querying the database after new tuples have been
  added. When reusing the state a considerably smaller amount of work
  is necessary. *)

  type state
  (** State of a Datalog evaluation. *)

  val init : Program.t -> state
  (** [init program] Returns the initial state for evaluations with
  the Datalog program [program]. *)

  val query_with_state :
    database:Database.t -> state:state -> query -> (state * Tuple.Set.t) Lwt.t
  (** [query_with_state ~database ~state query] returns the set of
  answer tuples for [query] (see also {!query}) as well as the updated
  state.

  The set of answer tuples for a query is monotonically increasing
  with the updated state.

  The database must also be monotonically increasing (tuples can not
  be deleted from the database). When new tuples are added to the
  database, they will be considered for answers to the query. But if
  tuples are removed from the databse the returned answer might
  contain tuples that are no longer valid with respect to the program
  and database. *)
end
