(*
 * SPDX-FileCopyrightText: 2021, 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** Input signature of the functor {!Make}. *)
module type CONSTANT = sig
  include Constant.S
end

(** Output signature of the functor {!Make}. *)
module type S = sig
  include Datalogl_intf.Sigs
end

(** Functor building a Datalog implementation for the given constant type. *)
module Make (Constant : CONSTANT) : S with type constant = Constant.t
