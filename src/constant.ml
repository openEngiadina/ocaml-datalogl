(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module type S = sig
  (** Input signature of the functor {!Make}. *)

  type t
  (** The type of constants. *)

  val compare : t -> t -> int
  (** A total ordering function over the keys. See also {!Stdlib.compare}. *)

  (** {1 IO} *)

  val parser : t Angstrom.t
  (** [parser] is a parser that reads a single constant. This is used
  for parsing complex Datalog expressions. *)

  val pp : t Fmt.t
end
