(*
 * SPDX-FileCopyrightText: 2021, 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax

let src = Logs.Src.create "Datalogl"

module Log = (val Logs.src_log src : Logs.LOG)

module type CONSTANT = sig
  include Constant.S
end

module type S = sig
  include Datalogl_intf.Sigs
end

module Make (Constant : CONSTANT) = struct
  (* Constant *)

  type constant = Constant.t

  let constant_equal a b = Int.equal 0 @@ Constant.compare a b
  (* Debugging *)

  (* To debug evaluation it is helpful to output lot of
     details. However, this clobbers up output when used in conjunction
     with other libraries (and debugging them). We allow debugging to be
     activiated manuall (instead of learning how to do it nicely with the
     Logs library - TODO). *)

  let _debug_enabled = ref false
  let set_debug b = _debug_enabled := b
  let debug f = if !_debug_enabled then Log.debug f else () [@@inline]

  (* Basic parsers *)

  let parser_whitespace =
    Angstrom.(
      many @@ choice [ string " "; string "\n"; string "\t" ] >>| ignore)

  let parser_symbol =
    Angstrom.(
      take_while (fun c ->
          let code = Char.code c in
          (* ASCII upper case *)
          (65 <= code && code <= 90)
          (* ASCII lower case *)
          || (97 <= code && code <= 122)
          (* ASCII numbers  *)
          || (48 <= code && code <= 57)
          (* hyphen, minus sign *)
          || code = 45
          (* underscore *)
          || code = 95))

  let parser_comma =
    Angstrom.(parser_whitespace *> char ',' <* parser_whitespace)

  (* General Logic Programming Types  *)

  module Variable = struct
    type t = string

    let make id = id
    let fresh_counter = ref 0

    let make_fresh () =
      let n = !fresh_counter in
      fresh_counter := n + 1;
      "var-" ^ string_of_int n

    let compare = String.compare
    let parser = Angstrom.(char '?' *> parser_symbol)
    let pp ppf id = Fmt.pf ppf "?%s" id

    module Set = Set.Make (String)
    module Map = Map.Make (String)
  end

  module Term = struct
    type t = Variable of Variable.t | Constant of constant

    let make_variable s = Variable s
    let make_constant c = Constant c

    let equal a b =
      match (a, b) with
      | Variable id_a, Variable id_b -> String.equal id_a id_b
      | Constant c_a, Constant c_b -> constant_equal c_a c_b
      | _ -> false

    let compare a b =
      match (a, b) with
      | Variable id_a, Variable id_b -> String.compare id_a id_b
      | Constant c_a, Constant c_b -> Constant.compare c_a c_b
      | Variable _, Constant _ -> -1
      | Constant _, Variable _ -> 1

    let map f_variable f_constant = function
      | Variable id -> f_variable id
      | Constant c -> f_constant c

    let is_constant term = map (fun _ -> false) (fun _ -> true) term
    let is_variable term = map (fun _ -> true) (fun _ -> false) term
    let parser_variable = Angstrom.(char '?' *> parser_symbol)

    let parser =
      Angstrom.(
        choice
          [
            parser_variable >>| make_variable; Constant.parser >>| make_constant;
          ])

    let pp ppf term =
      Fmt.pf ppf "@[<h>%s@]"
        (map
           (fun id -> "?" ^ id)
           (fun value -> Fmt.to_to_string Constant.pp value)
           term)
  end

  module Substitution = struct
    module TermMap = Map.Make (Term)

    type t = Term.t TermMap.t

    let empty = TermMap.empty

    let bind variable term substitution =
      match variable with
      | Term.Variable _ -> TermMap.add variable term substitution
      | _ -> substitution

    let deref substitution term =
      match TermMap.find_opt term substitution with
      | Some bound -> bound
      | None -> term

    let unify_term ?(substitution = empty) a b : t option =
      let a = deref substitution a in
      let b = deref substitution b in
      match (a, b) with
      | Term.Variable _, Term.Variable _ when Term.equal a b ->
          Some substitution
      | Term.Variable _, _ -> Some (bind a b substitution)
      | _, Term.Variable _ -> Some (bind b a substitution)
      | Term.Constant a_value, Term.Constant b_value ->
          if constant_equal a_value b_value then Some substitution else None

    (* let pp ppf substitution =
     *   let pp_pair ppf (a, b) = Fmt.pf ppf "@[@ %a → %a@]" Term.pp a Term.pp b in
     *   Fmt.(
     *     pf ppf "@[<6><subst%a>@]" (seq pp_pair) (TermMap.to_seq substitution)) *)
  end

  module Atom = struct
    type predicate_symbol = string
    type t = { predicate : predicate_symbol; terms : Term.t list }

    let make predicate terms = { predicate; terms }
    let predicate atom = atom.predicate
    let terms atom = atom.terms

    let equal a b =
      String.equal (predicate a) (predicate b)
      && List.for_all2 Term.equal (terms a) (terms b)

    let equal_predicate a b =
      String.equal (predicate a) (predicate b)
      && List.length (terms a) = List.length (terms b)

    let compare a b =
      if String.equal (predicate a) (predicate b) then
        List.compare Term.compare (terms a) (terms b)
      else String.compare (predicate a) (predicate b)

    (* let unify ?(substitution = Substitution.empty) a b =
     *   if equal_predicate a b then
     *     List.fold_left2
     *       (fun substitution_opt term_a term_b ->
     *         Option.bind substitution_opt (fun substitution ->
     *             Substitution.unify_term ~substitution term_a term_b))
     *       (Some substitution) a.terms b.terms
     *   else None *)

    (* let equal_alpha a b = Option.is_some @@ unify a b *)

    (* let substitute substitution atom =
     *   make atom.predicate
     *     (List.map (fun term -> Substitution.deref substitution term) atom.terms) *)

    let is_ground atom = List.for_all Term.is_constant atom.terms

    (* let fresh_substitution ?(substitution = Substitution.empty) atom =
     *   List.fold_left
     *     (fun substitution term ->
     *       if Term.is_variable term then
     *         Substitution.bind term
     *           (Term.make_variable @@ Variable.make_fresh ())
     *           substitution
     *       else substitution)
     *     substitution atom.terms *)

    let parser =
      Angstrom.(
        return make <*> parser_symbol <* char '(' <* parser_whitespace
        <*> sep_by1 parser_comma Term.parser
        <* parser_whitespace <* char ')')

    let pp ppf atom =
      Fmt.pf ppf "@[<2>%s(%a)@]" atom.predicate
        Fmt.(list ~sep:comma Term.pp)
        atom.terms
  end

  module Literal = struct
    type t = Positive of Atom.t | Negative of Atom.t
    type t' = t

    let make_positive atom = Positive atom
    let make_negative atom = Negative atom
    let to_atom = function Positive atom -> atom | Negative atom -> atom
    let predicate literal = Atom.predicate @@ to_atom literal
    let terms literal = Atom.terms @@ to_atom literal

    let compare a b =
      match (a, b) with
      | Positive atom_a, Positive atom_b -> Atom.compare atom_a atom_b
      | Negative atom_a, Negative atom_b -> Atom.compare atom_a atom_b
      | Positive _, Negative _ -> -1
      | Negative _, Positive _ -> 1

    let equal a b = 0 = compare a b
    let is_positive = function Positive _ -> true | Negative _ -> false
    let is_ground literal = Atom.is_ground @@ to_atom literal

    (* let substitute substitution = function
     *   | Positive atom -> Positive (Atom.substitute substitution atom)
     *   | Negative atom -> Negative (Atom.substitute substitution atom) *)

    let parser =
      Angstrom.(
        choice
          [
            char '~' *> Atom.parser >>| make_negative;
            Atom.parser >>| make_positive;
          ])

    let pp ppf = function
      | Positive atom -> Atom.pp ppf atom
      | Negative atom -> Fmt.pf ppf "@[<1>~%a@]" Atom.pp atom

    module Set = Set.Make (struct
      type t = t'

      let compare = compare
    end)
  end

  module Clause = struct
    type t = { head : Atom.t; body : Literal.Set.t }
    type clause = t

    let make head body = { head; body }
    let make_fact head = make head Literal.Set.empty
    let head clause = clause.head
    let body clause = clause.body

    let compare a b =
      if Atom.compare a.head b.head = 0 then Literal.Set.compare a.body b.body
      else Atom.compare a.head b.head

    let equal a b = compare a b = 0

    let is_ground_fact clause =
      Literal.Set.is_empty clause.body && Atom.is_ground clause.head

    (* let fresh clause =
     *   let substitution =
     *     List.fold_left
     *       (fun substitution atom -> Atom.fresh_substitution ~substitution atom)
     *       Substitution.empty
     *       (clause.head :: List.map Literal.to_atom clause.body)
     *   in
     *   make
     *     (Atom.substitute substitution clause.head)
     *     (List.map (Literal.substitute substitution) clause.body) *)

    module Set = Set.Make (struct
      type t = clause

      let compare = compare
    end)

    let parser_fact =
      Angstrom.(Atom.parser <* parser_whitespace <* char '.' >>| make_fact)

    let parser_rule =
      Angstrom.(
        return make <*> Atom.parser <* parser_whitespace <* string ":-"
        <* parser_whitespace
        <*> (sep_by1 parser_comma Literal.parser >>| Literal.Set.of_list)
        <* parser_whitespace <* char '.')

    let parser = Angstrom.(choice [ parser_fact; parser_rule ])

    let pp ppf clause =
      if Literal.Set.is_empty @@ body clause then
        Fmt.pf ppf "@[%a.@]" Atom.pp (head clause)
      else
        Fmt.pf ppf "@[%a@ :-@ %a.@]" Atom.pp (head clause)
          Fmt.(seq ~sep:comma Literal.pp)
          (Literal.Set.to_seq @@ body clause)
  end

  module Program = struct
    type t = Clause.Set.t

    let empty = Clause.Set.empty
    let add = Clause.Set.add
    let add_seq = Clause.Set.add_seq

    let predicate_clauses idb predicate =
      Clause.Set.to_seq idb
      |> Seq.filter (fun clause ->
             String.equal predicate (Atom.predicate @@ Clause.head clause))

    let is_idb_predicate idb predicate =
      match
        Clause.Set.find_first_opt
          (* find_first_opt requires a monotonically increasing
             function. I assume this allows a more efficient search in
             the Set. *)
            (fun clause ->
            let clause_predicate = Atom.predicate @@ Clause.head clause in
            String.compare predicate clause_predicate <= 0)
          idb
      with
      | Some clause ->
          String.equal (Atom.predicate @@ Clause.head clause) predicate
      | None -> false

    let parser =
      Angstrom.(
        (fun clauses -> empty |> add_seq (List.to_seq clauses))
        <$> parser_whitespace *> sep_by parser_whitespace Clause.parser
        <* parser_whitespace)

    let pp ppf idb =
      Fmt.pf ppf "@[<5><IDB %a>@]"
        (Fmt.seq ~sep:Fmt.sp Clause.pp)
        (Clause.Set.to_seq idb)
  end

  (* EDB and Relational Algebra *)

  module Tuple = struct
    type t = constant list
    type tuple = t

    let unify_with_terms ?(substitution = Substitution.empty) terms tuple =
      List.fold_left2
        (fun substitution_opt element term ->
          Option.bind substitution_opt (fun substitution ->
              Substitution.unify_term ~substitution
                (Term.make_constant element)
                term))
        (Some substitution) tuple terms

    let equal_alpha_with_terms terms tuple =
      Option.is_some @@ unify_with_terms terms tuple

    let pp = Fmt.(brackets @@ list ~sep:comma Constant.pp)

    module Set = struct
      include Set.Make (struct
        type t = tuple

        let compare = List.compare Constant.compare
      end)

      let pp ppf set =
        Fmt.pf ppf "%a" Fmt.(braces @@ seq ~sep:comma pp) (to_seq set)
    end
  end

  (* Helpers for dealing with attributes of relations *)
  module Attributes = struct
    type t = string list

    type lookup_map = int list
    (** A lookup map can be used to get specific values in a specific
    order from tuples *)

    (* Returns the common attributes of relations a and b in a stable order *)
    let common left right =
      Variable.Set.(
        List.of_seq @@ to_seq @@ inter (of_list left) (of_list right))

    (* Returns the lookup map for getting attributes [selection] from
       the attributes [from]. *)
    let lookup_map (selection : t) (from : t) : lookup_map =
      let indexed = List.mapi (fun i a -> (a, i)) from in
      List.map
        (fun attribute ->
          match
            List.find_opt (fun (a, _) -> String.equal attribute a) indexed
          with
          | Some (_, i) -> i
          | None ->
              failwith
              @@ Format.sprintf
                   "can not build attribute index: attribute %s does not \
                    appear."
                   attribute)
        selection

    let lookup (lookup_map : lookup_map) (tuple : Tuple.t) : Tuple.t =
      List.map (fun pos -> List.nth tuple pos) lookup_map

    let pp = Fmt.(brackets @@ list ~sep:comma Variable.pp)
  end

  module Relation = struct
    type attributes = string list
    type t = attributes * Tuple.Set.t

    (* Constructors *)
    let make ~attributes tuples : t = (attributes, tuples)
    let empty attributes = make ~attributes Tuple.Set.empty

    (* Getters *)

    let attributes (attributes, _) = attributes
    let tuples (_, tuples) = tuples
    let is_empty (_, tuples) = Tuple.Set.is_empty tuples

    (* Add tuples to the relation *)
    let add tuple (attributes, tuples) = (attributes, Tuple.Set.add tuple tuples)

    (* Helpers for looking up specific attribute values in tuples *)

    (* Returns a map that can be used to locate values in tuples *)
    let attribute_index (attributes, _) =
      attributes
      |> List.mapi (fun i var -> (var, i))
      |> List.to_seq |> Variable.Map.of_seq

    let project ~attributes (in_attributes, in_tuples) =
      let lookup_map = Attributes.lookup_map attributes in_attributes in
      (attributes, Tuple.Set.map (Attributes.lookup lookup_map) in_tuples)

    let pp ppf (attributes, tuples) =
      Fmt.(
        pf ppf "@[<4><rel %a %a>@]" Attributes.pp attributes Tuple.Set.pp tuples)
  end

  module Database = struct
    type pattern = constant option list

    let pp_pattern =
      Fmt.(brackets @@ list ~sep:comma @@ option ~none:(any "?") @@ Constant.pp)

    type t = Atom.predicate_symbol -> pattern -> Tuple.t Lwt_seq.t

    let empty _ _ = Lwt_seq.empty

    let select pattern tuples =
      Lwt_seq.filter
        (fun tuple ->
          List.for_all2
            (fun value constraint_opt ->
              match constraint_opt with
              | Some constraint' -> Constant.compare value constraint' = 0
              | None -> true)
            tuple pattern)
        tuples

    module Relation = struct
      (* Define a relation to simplify handling of attributes *)

      type t = Attributes.t * Tuple.t Lwt_seq.t

      let attributes (attributes, _) = attributes
      let tuples (_, tuples) = tuples

      (* Constructors *)
      let make attributes tuples = (attributes, tuples)

      let of_memory (relation : Relation.t) : t =
        Relation.tuples relation |> Tuple.Set.to_seq |> Lwt_seq.of_seq
        |> make (Relation.attributes relation)

      (* Relational Algebra queries *)

      let project projection_attributes ((tuple_attributes, tuples) : t) : t =
        let lookup_map =
          Attributes.lookup_map projection_attributes tuple_attributes
        in
        Lwt_seq.map (Attributes.lookup lookup_map) tuples
        |> make projection_attributes

      (* Nested Loop Join *)

      let natural_join (left : Relation.t) (right : t) : t =
        let left_attributes = Relation.attributes left in
        let right_attributes = attributes right in

        (* find the common attributes *)
        let common_attributes =
          Attributes.common left_attributes right_attributes
        in

        (* lookup maps for finding common attributes *)
        let left_lookup_map =
          Attributes.lookup_map common_attributes left_attributes
        in
        let right_lookup_map =
          Attributes.lookup_map common_attributes right_attributes
        in

        (* attributes of output relation *)
        let out_attributes =
          left_attributes @ right_attributes |> List.sort_uniq Variable.compare
        in

        (* lookup map to find output attributes *)
        let out_lookup_map =
          Attributes.lookup_map out_attributes
            (left_attributes @ right_attributes)
        in

        (* go over all tuples in right side *)
        Lwt_seq.flat_map
          (fun right ->
            (* get common attributes from right side *)
            let right_common = Attributes.lookup right_lookup_map right in

            (* loop over the left side *)
            Tuple.Set.to_seq @@ Relation.tuples left
            |> Seq.filter_map (fun left ->
                   (* get common attributes from left *)
                   let left_common = Attributes.lookup left_lookup_map left in

                   if List.compare Constant.compare left_common right_common = 0
                   then
                     (* if common values are same then output joined tuple *)
                     Option.some
                     @@ Attributes.lookup out_lookup_map (left @ right)
                   else None)
            |> Lwt_seq.of_seq)
          (tuples right)
        (* return as a relation *)
        |> make out_attributes

      (* to a Relation.t relation, to be precise *)
      let to_memory (attributes, tuples) =
        Lwt_seq.fold_left
          (fun rel tuple -> Relation.add tuple rel)
          (Relation.empty attributes)
          tuples
    end
  end

  (* QSQ *)

  module QSQ = struct
    module Adornment = struct
      type t = bool list
      (** Adornment are represented as lists of bools where true
      indicates that the coordinate is bound. *)

      let compare = List.compare Bool.compare

      let of_atom ?(bound_variables = Variable.Set.empty) atom =
        Atom.terms atom
        |> List.map (function
             | Term.Variable variable ->
                 Variable.Set.mem variable bound_variables
             | Term.Constant _ -> true)

      let bound_ratio adornment =
        List.map (function true -> 1.0 | false -> 0.0) adornment
        |> List.fold_left ( +. ) 0.0
        |> fun sum -> sum /. (float_of_int @@ List.length adornment)

      let is_completely_free adornment = List.for_all not adornment

      let of_literal ?(bound_variables = Variable.Set.empty) literal =
        Literal.terms literal
        |> List.map (function
             | Term.Variable variable ->
                 Variable.Set.mem variable bound_variables
             | Term.Constant _ -> true)

      let pp ppf adornment =
        Fmt.pf ppf "@[%a@]"
          Fmt.(list ~sep:nop string)
          (List.map (function true -> "b" | false -> "f") adornment)
    end

    module PredicateAdornment = struct
      type t = Atom.predicate_symbol * Adornment.t
      type t' = t

      let compare (p_a, ad_a) (p_b, ad_b) =
        if String.compare p_a p_b = 0 then Adornment.compare ad_a ad_b
        else String.compare p_a p_b

      module Map = Map.Make (struct
        type t = t'

        let compare = compare
      end)
    end

    module SubQuery = struct
      type t = {
        predicate : Atom.predicate_symbol;
        adornment : Adornment.t;
        bindings : Tuple.Set.t;
      }

      let equal a b =
        String.equal a.predicate b.predicate
        && Adornment.compare a.adornment b.adornment = 0
        && Tuple.Set.equal a.bindings b.bindings

      let create predicate adornment bindings =
        { predicate; adornment; bindings }

      let of_atom atom =
        {
          predicate = Atom.predicate atom;
          adornment = Adornment.of_atom atom;
          bindings =
            Tuple.Set.empty
            |> Tuple.Set.add
                 (Atom.terms atom
                 |> List.filter_map (function
                      | Term.Constant constant -> Some constant
                      | Term.Variable _ -> None));
        }

      let pp ppf subquery =
        Fmt.pf ppf "@[<><subquery %s %a (bindings: %a)>@]" subquery.predicate
          Adornment.pp subquery.adornment Tuple.Set.pp subquery.bindings
    end

    module ClauseAdornment = struct
      type t = Clause.t * Adornment.t
      type t' = t

      let compare (clause_a, ad_a) (clause_b, ad_b) =
        if Clause.compare clause_a clause_b = 0 then Adornment.compare ad_a ad_b
        else Clause.compare clause_a clause_b

      module Map = Map.Make (struct
        type t = t'

        let compare = compare
      end)
    end

    (* List helpers *)

    let zip a b = List.map2 (fun a b -> (a, b)) a b

    let unzip a =
      let l_rev, r_rev =
        List.fold_left
          (fun (a_l, b_l) (a, b) -> (a :: a_l, b :: b_l))
          ([], []) a
      in
      (List.rev l_rev, List.rev r_rev)

    (* Return the attributes of the next sup. This just removes all
       constant variables that vwere introduced by the literal. *)
    let next_sup_attributes sup_attributes literal : Relation.attributes =
      sup_attributes
      @ List.filter_map
          (function Term.Constant _ -> None | Term.Variable var -> Some var)
          (Literal.terms literal)
      |> List.sort_uniq String.compare

    module Template = struct
      type entry = {
        literal : Literal.t;
        adornment : Adornment.t;
        literal_attributes : Relation.attributes;
      }

      type t = {
        (* The head of the clause. *)
        head : Atom.t;
        (* attributes appearing literal head *)
        head_attributes : Relation.attributes;
        (* attributes bound by subquery *)
        subquery_attributes : Relation.attributes;
        (* list of structures required for processing literals appearing in clause. *)
        body : entry list;
      }

      let pp ppf template =
        Fmt.pf ppf "%a"
          Fmt.(list ~sep:semi @@ pair ~sep:comma Literal.pp Adornment.pp)
          (List.map
             (fun entry -> (entry.literal, entry.adornment))
             template.body)

      (** Make a template for a given clause and adornment. *)
      let make clause adornment =
        (* The attributes/variables appearing in the head of the
           clause. All attributes appear in the final sup. The final step
           of processing a clause is to project onto these attributes. *)
        let head_attributes =
          List.map
            (function
              | Term.Variable var -> var
              | Term.Constant _ -> Variable.make_fresh ())
            (Atom.terms @@ Clause.head clause)
        in

        let subquery_attributes =
          List.filter_map
            (fun (is_bound, attribute) ->
              if is_bound then Some attribute else None)
            (zip adornment head_attributes)
        in

        (* All attributes bound by subquery plus variables introduced
           for constants appearing in head. *)
        let sup0_attributes =
          subquery_attributes
          @ List.filter_map
              (fun (term, attribute) ->
                if Term.is_constant term then Some attribute else None)
              (zip (Atom.terms @@ Clause.head clause) head_attributes)
        in

        let literal_adornment sup_attributes literal =
          Adornment.of_literal
            ~bound_variables:(Variable.Set.of_list sup_attributes)
            literal
        in

        let make_entry sup_attributes literal =
          let literal_attributes =
            List.map
              (function
                | Term.Variable v -> v
                | Term.Constant _ -> Variable.make_fresh ())
              (Literal.terms literal)
          in

          let adornment = literal_adornment sup_attributes literal in

          { literal; adornment; literal_attributes }
        in

        let select_best_literal sup_attributes literals =
          Literal.Set.to_seq literals
          |> Seq.fold_left
               (fun (max_ratio, current_max) literal ->
                 let bound_ratio =
                   Adornment.bound_ratio
                   @@ literal_adornment sup_attributes literal
                 in
                 if max_ratio <= bound_ratio then (bound_ratio, Some literal)
                 else (max_ratio, current_max))
               (0.0, None)
          |> snd
        in

        let rec build_template_body sup_attributes body literals =
          if Literal.Set.is_empty literals then body
          else
            let literal =
              select_best_literal sup_attributes literals
              (* As Literal.Set is non empty we can be sure the option will be Some. *)
              |> Option.get
            in

            let entry = make_entry sup_attributes literal in
            let next_sup_attributes =
              next_sup_attributes sup_attributes literal
            in
            build_template_body next_sup_attributes (body @ [ entry ])
              (Literal.Set.remove literal literals)
        in

        let body =
          build_template_body sup0_attributes [] (Clause.body clause)
        in

        {
          head = Clause.head clause;
          head_attributes;
          subquery_attributes;
          body;
        }
    end

    module State = struct
      type t = {
        input : Tuple.Set.t PredicateAdornment.Map.t;
        answer : Tuple.Set.t PredicateAdornment.Map.t;
        templates : Template.t ClauseAdornment.Map.t;
      }

      let empty =
        {
          input = PredicateAdornment.Map.empty;
          answer = PredicateAdornment.Map.empty;
          templates = ClauseAdornment.Map.empty;
        }

      let compare a b =
        let input_compare =
          PredicateAdornment.Map.compare Tuple.Set.compare a.input b.input
        in
        if input_compare = 0 then
          PredicateAdornment.Map.compare Tuple.Set.compare a.answer b.answer
        else input_compare

      let answers predicate adornment state =
        PredicateAdornment.Map.find_opt (predicate, adornment) state.answer
        |> Option.value ~default:Tuple.Set.empty

      (* Unify answers with query terms. This prevents answers that do
         not match queries with repeated variable occurences. *)
      let unified_answers predicate adornment query state =
        answers predicate adornment state
        |> Tuple.Set.filter (Tuple.equal_alpha_with_terms (Atom.terms query))

      let inputs predicate adornment state =
        PredicateAdornment.Map.find_opt (predicate, adornment) state.input
        |> Option.value ~default:Tuple.Set.empty

      let add_inputs predicate adornment inputs state =
        {
          state with
          input =
            PredicateAdornment.Map.(
              state.input
              |> merge
                   (fun _predicate_adornment old new' ->
                     match (old, new') with
                     | None, Some new' -> Some new'
                     | Some old, None -> Some old
                     | Some old, Some new' ->
                         Option.some @@ Tuple.Set.union old new'
                     | None, None -> None)
                   (singleton (predicate, adornment) inputs));
        }

      let add_answers predicate adornment answers state =
        {
          state with
          answer =
            PredicateAdornment.Map.(
              state.answer
              |> merge
                   (fun _predicate_adornment old new' ->
                     match (old, new') with
                     | None, Some new' -> Some new'
                     | Some old, None -> Some old
                     | Some old, Some new' ->
                         Option.some @@ Tuple.Set.union old new'
                     | None, None -> None)
                   (singleton (predicate, adornment) answers));
        }
    end

    (* Return the initial supplementary relation. *)
    let sup0_of_head (subquery : SubQuery.t) (template : Template.t) =
      (* Create a in-memory relation for the subquery bindings. *)
      let subquery_relation =
        Relation.make ~attributes:template.subquery_attributes subquery.bindings
      in

      (* Create a singleton (single row) relation for the constants
         appearing in head. *)
      let head_constants_variables, head_constants_values =
        unzip
        @@ List.filter_map
             (fun (term, variable) ->
               match term with
               | Term.Constant constant -> Some (variable, constant)
               | Term.Variable _ -> None)
             (zip (Atom.terms template.head) template.head_attributes)
      in
      let head_constants =
        Relation.make ~attributes:head_constants_variables
          (Tuple.Set.singleton head_constants_values)
      in

      (* The initial supplementary relation is the join on the
         constants appearing in the haed. *)
      let* sup_0 =
        Database.Relation.(
          of_memory subquery_relation
          |> natural_join head_constants
          |> to_memory)
      in

      return sup_0

    (* Return set of tuples that are bound by sup in literal *)
    let bound_tuples sup literal =
      let variable_index = Relation.attribute_index sup in
      let ref_map =
        List.filter_map
          (function
            | Term.Constant c -> Option.some @@ Either.left c
            | Term.Variable var ->
                Variable.Map.find_opt var variable_index
                |> Option.map Either.right)
          (Literal.terms literal)
      in
      Tuple.Set.map
        (fun tuple ->
          List.map
            (function Either.Left c -> c | Either.Right i -> List.nth tuple i)
            ref_map)
        (Relation.tuples sup)

    (* Factorize EDB query by using tuples from sup. This causes
       multiple but probably smaller EDB queries. *)
    let factorize_edb_queries ~edb predicate literal_attributes
        base_query_pattern sup =
      (* Attach an index to sup attributes *)
      let indexed_sup =
        List.mapi (fun i a -> (a, i)) (Relation.attributes sup)
      in
      (* create a lookup map for finding position in sup tuple for
         common attribute. This is very similar to Attributes.lookup_map but
         also handles the case where lookup fails. *)
      let query_lookup_map =
        List.map
          (fun attribute ->
            match
              List.find_opt (fun (a, _) -> String.equal attribute a) indexed_sup
            with
            | Some (_, i) -> Some i
            | None -> None)
          literal_attributes
      in

      (* Iterate over all tuples in sup *)
      Relation.tuples sup |> Tuple.Set.to_seq |> Lwt_seq.of_seq
      (* For every sup tuple *)
      |> Lwt_seq.flat_map (fun sup_tuple ->
             (* create a query pattern with additional constant values from the sup tuple. *)
             let query_pattern =
               List.map
                 (fun (sup_pos_opt, query_pattern) ->
                   match sup_pos_opt with
                   | Some sup_pos -> Option.some @@ List.nth sup_tuple sup_pos
                   | None -> query_pattern)
                 (zip query_lookup_map base_query_pattern)
             in

             debug (fun m ->
                 m "EDB (factorized): %s(%a)" predicate Database.pp_pattern
                   query_pattern);

             (* Run the EDB query with the refined query pattern *)
             edb predicate query_pattern)

    let rec qsqr ~edb ~idb ~state (query : SubQuery.t) =
      (* If subquery has bound variables but no bindings we can
         immediately return.

         This may happen when bindings have already been processed and
         are in the inputs. process_clause_body_literal will remove them
         from the bindings before calling qsqr recursively. *)
      if
        List.exists (fun x -> x) query.adornment
        && Tuple.Set.is_empty query.bindings
      then return state
      else
        (* get matching clauses *)
        let clauses =
          Program.predicate_clauses idb query.predicate |> List.of_seq
        in
        debug (fun m -> m "QSQR %a" SubQuery.pp query);

        (* evaluate all matching clauses *)
        let* new_state =
          Lwt_list.fold_left_s
            (fun state clause ->
              process_subquery_rule ~edb ~idb ~state query clause)
            state clauses
        in

        (* return if state has not changed *)
        if State.compare state new_state = 0 then return state
        else (* or repeat otherwise *)
          qsqr ~edb ~idb ~state:new_state query

    and process_clause_body_idb_literal ~edb ~idb ~state ~subquery sup
        (template_entry : Template.entry) =
      let literal = template_entry.literal in
      let predicate = Literal.predicate literal in
      let adornment = template_entry.adornment in

      debug (fun m ->
          m "processing IDB literal: %a %a (sup: %a)" Literal.pp literal
            Adornment.pp adornment Relation.pp sup);

      (* S = sup[bound(R)] - input_R *)
      let s =
        Tuple.Set.diff
          (bound_tuples sup template_entry.literal)
          (State.inputs predicate adornment state)
      in

      (* add s to input *)
      let state = State.add_inputs predicate adornment s state in

      (* Create a subquery for predicate *)
      let subquery' = SubQuery.create predicate adornment s in

      (* recursively evaluate subquery *)
      let* state =
        if SubQuery.equal subquery subquery' then return state
        else qsqr ~edb ~idb ~state subquery'
      in

      let answers =
        Relation.make ~attributes:template_entry.literal_attributes
        @@ State.unified_answers predicate adornment (Literal.to_atom literal)
             state
      in

      let* next_sup =
        Database.Relation.(
          of_memory answers |> natural_join sup
          |> project (next_sup_attributes (Relation.attributes sup) literal)
          |> to_memory)
      in

      (* debug (fun m ->
       *     m "completed processing of IDB literal: %a %a (next_sup: %a)"
       *       Literal.pp literal Adornment.pp adornment Relation.pp next_sup); *)
      return (state, next_sup)

    and process_clause_body_edb_literal ~edb ~state sup
        (template_entry : Template.entry) =
      let literal = template_entry.literal in

      debug (fun m ->
          m "processing EDB literal: %a (sup: %a)" Literal.pp literal
            Relation.pp sup);

      let query_pattern =
        List.map
          (function Term.Constant c -> Some c | Term.Variable _ -> None)
          (Literal.terms literal)
      in

      (* get matching tuples from EDB *)
      let edb_tuples =
        if Adornment.is_completely_free template_entry.adornment then
          (* Run an unrestricted EDB query. This should not happen
             often as it is very expensive. *)
          let () =
            debug (fun m ->
                m "EDB (unrestricted): %s(%a)"
                  (Literal.predicate literal)
                  Database.pp_pattern query_pattern)
          in
          edb (Literal.predicate literal) query_pattern
        else
          (* Factorize the EDB query with bound variables in sup. This
             results in multiple but smaller EDB queries. *)
          factorize_edb_queries ~edb
            (Literal.predicate literal)
            template_entry.literal_attributes query_pattern sup
      in

      let* next_sup =
        Database.Relation.(
          edb_tuples
          (* annotate with attributes *)
          |> make template_entry.literal_attributes
          (* join with previous sup *)
          |> natural_join sup
          (* project to attributes of next sup (this gets rid of
             unnecessary constant variables) *)
          |> project (next_sup_attributes (Relation.attributes sup) literal)
          (* Store tuples into a new in-memory relation *)
          |> to_memory)
      in

      (* debug (fun m ->
       *     m "completed processing of EDB literal: %a (next_sup: %a)" Literal.pp
       *       literal Relation.pp next_sup); *)
      return (state, next_sup)

    and process_subquery_rule ~edb ~idb ~state (subquery : SubQuery.t) clause =
      (* Get template for the clause/adornment and add template to
         state if we need to create one. *)
      let template, state =
        match
          ClauseAdornment.Map.find_opt
            (clause, subquery.adornment)
            state.templates
        with
        | Some template -> (template, state)
        | None ->
            (* Make new template if there is none for the clause/adornment yet. *)
            let template = Template.make clause subquery.adornment in

            debug (fun m ->
                m "Template for %a (%a): %a" Clause.pp clause Adornment.pp
                  subquery.adornment Template.pp template);

            ( template,
              {
                state with
                templates =
                  ClauseAdornment.Map.add
                    (clause, subquery.adornment)
                    template state.templates;
              } )
      in

      (* Compute initial supplementary relation *)
      let* sup_0 = sup0_of_head subquery template in

      (* debug (fun m ->
       *     m "processing rule: %a (SubQuery: %a) (sup_0: %a)" Clause.pp clause
       *       SubQuery.pp subquery Relation.pp sup_0); *)
      let* state, sup_final =
        Lwt_list.fold_left_s
          (fun (state, sup) (template_entry : Template.entry) ->
            debug (fun m -> m " sup: %a" Relation.pp sup);
            if
              Program.is_idb_predicate idb
                (Literal.predicate template_entry.literal)
            then
              process_clause_body_idb_literal ~edb ~idb ~state ~subquery sup
                template_entry
            else process_clause_body_edb_literal ~edb ~state sup template_entry)
          (state, sup_0) template.body
      in

      if Relation.is_empty sup_final then return state
      else
        (* Project the final supplementary relation to answers *)
        let answers =
          Relation.(project ~attributes:template.head_attributes @@ sup_final)
        in

        (* debug (fun m ->
         *     m "New answers added because of rule %a (subquery %a) (sup: %a): %a"
         *       Clause.pp clause SubQuery.pp subquery Relation.pp sup_final
         *       Tuple.Set.pp (Relation.tuples answers)); *)

        (* Add tuples from final supplementary relation to answers *)
        return
        @@ State.add_answers subquery.predicate subquery.adornment
             (Relation.tuples answers) state
  end

  type query = Atom.t

  (* Incremental Evanluation *)

  type state = QSQ.State.t * Program.t

  (* In the future this might include some pre-optimization on the program. *)
  let init program = (QSQ.State.empty, program)

  let query_with_state ~database ~state query =
    let state, program = state in

    if Program.is_idb_predicate program (Atom.predicate query) then
      let subquery = QSQ.SubQuery.of_atom query in
      let* state' = QSQ.qsqr ~edb:database ~idb:program ~state subquery in
      let answers =
        QSQ.State.unified_answers subquery.predicate subquery.adornment query
          state'
      in
      return ((state', program), answers)
    else
      (* Allow queries on EDB predicates.

         This is a deviation from pure Datalog as described in the
         literature where queries are always for IDB predicates. We
         allow this for quickly querying the EDB directly with the
         same API. A query for an EDB predicate is not really a
         Datalog query.

         This will never be called from a recursive call as QSQR
         never creates subqueries for EBD predicates.
      *)
      let query_pattern =
        List.map
          (function Term.Constant c -> Some c | Term.Variable _ -> None)
          (Atom.terms query)
      in

      let* tuples =
        database (Atom.predicate query) query_pattern
        |> Lwt_seq.fold_left
             (fun set tuple -> Tuple.Set.add tuple set)
             Tuple.Set.empty
      in

      return ((state, program), tuples)

  let query ~database ~program query =
    let state = init program in
    let* _state, answers = query_with_state ~database ~state query in
    return answers
end
