open Lwt
open Lwt.Syntax

module Constant = struct
  type t = int

  let equal = Int.equal
  let compare = Int.compare
  let hash v = v

  let parser =
    Angstrom.(
      take_while1 (fun c ->
          let code = Char.code c in
          48 <= code && code <= 57)
      >>| int_of_string)

  let pp = Fmt.int
end

module Datalog = Datalogl.Make (Constant)

let parse parser string =
  Angstrom.parse_string ~consume:Angstrom.Consume.All parser string
  |> Result.get_ok

let example =
  {example|
   p(?x,?y) :- e(?x,?y).
   p(?x,?y) :- e(?x,?z), p(?z,?y).
|example}

let main =
  Fmt_tty.setup_std_outputs ();
  Logs.set_reporter (Logs_fmt.reporter ());
  Logs.set_level ~all:true (Some Logs.Debug);

  Datalog.set_debug true;

  let program = parse Datalog.Program.parser example in

  let database = function
    | "e" ->
        fun pattern ->
          Lwt_seq.of_list [ [ 0; 1 ]; [ 1; 2 ]; [ 1; 3 ]; [ 2; 4 ]; [ 3; 4 ] ]
          |> Datalog.Database.select pattern
    | predicate_symbol -> Datalog.Database.empty predicate_symbol
  in

  let query = parse Datalog.Atom.parser "p(?a, ?b)" in

  let* solution = Datalog.query ~database ~program query in

  return
  @@ Format.printf "Answers: @[%a@]\n"
       (Fmt.seq ~sep:Fmt.sp Datalog.Tuple.pp)
       (Datalog.Tuple.Set.to_seq solution)

let () = Lwt_main.run main

(* let () =
 *   let a = parse Datalog.Clause.parser "p(?a,?c) :- e(?a,?b), p(?b,?c)." in
 *   Datalog.Clause.fresh a
 *   |> Format.printf "%a" Datalog.Clause.pp *)
