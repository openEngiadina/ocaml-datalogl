(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt

module Constant = struct
  type t = int

  let equal = Int.equal
  let compare = Int.compare
  let hash v = v

  let parser =
    Angstrom.(
      take_while1 (fun c ->
          let code = Char.code c in
          48 <= code && code <= 57)
      >>| int_of_string)

  let pp = Fmt.int
end

module Datalog = Datalogl.Make (Constant)

let main =
  let query =
    Datalog.RelationalAlgebra.(
      make_projection ~attributes:[ "a"; "b" ]
      @@ make_relation ~attributes:[ "a"; "b"; "c" ] "f")
  in
  let edb = function
    | "f" -> return @@ Datalog.Tuple.Set.of_list [ [ 1; 2; 3 ] ]
    | _ -> return @@ Datalog.Tuple.Set.empty
  in
  Datalog.RelationalAlgebra.eval ~edb query >>= fun tuples ->
  Lwt_fmt.printf "Query: %a\nAttributes: %a\nSolution: %a\n"
    Datalog.RelationalAlgebra.pp query Datalog.RelationalAlgebra.pp_attributes
    (Datalog.RelationalAlgebra.attributes query)
    Fmt.(seq Datalog.Tuple.pp)
    (Datalog.Tuple.Set.to_seq tuples)

let () = Lwt_main.run (main >>= fun () -> Lwt_fmt.flush Lwt_fmt.stdout)
