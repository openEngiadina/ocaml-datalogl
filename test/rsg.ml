(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax

module Datalog = Datalogl.Make (struct
  type t = string

  let compare = String.compare

  let parser =
    Angstrom.(
      take_while (fun c ->
          let code = Char.code c in
          (* ASCII upper case *)
          (65 <= code && code <= 90)
          (* ASCII lower case *)
          || (97 <= code && code <= 122)
          || (* ASCII numbers  *)
          (48 <= code && code <= 57)))

  let pp = Fmt.string
end)

let tuple_set_testable =
  Alcotest.testable Datalog.Tuple.Set.pp (fun a b ->
      Datalog.Tuple.Set.compare a b = 0)

let parse parser string =
  match Angstrom.parse_string ~consume:Angstrom.Consume.All parser string with
  | Ok v -> v
  | Error e -> failwith e

let make_test_case ~name ?(database = Datalog.Database.empty) ?expect idb query
    =
  Alcotest_lwt.test_case name `Quick (fun _switch () ->
      let program = parse Datalog.Program.parser idb in
      let query = parse Datalog.Atom.parser query in
      let* solution = Datalog.query ~database ~program query in
      match expect with
      | Some expect ->
          Alcotest.check tuple_set_testable
            "solution of Datalog evaluation matches expectation"
            (Datalog.Tuple.Set.of_list expect)
            solution;
          return_unit
      | None -> return_unit)

let rsg_program =
  {sgc|
   rsg(?x,?y) :- flat(?x,?y).
   rsg(?x,?y) :- up(?x,?x1), rsg(?y1,?x1), down(?y1,?y).
   |sgc}

let rsg_database = function
  | "up" ->
      fun pattern ->
        Lwt_seq.of_list
          [
            [ "a"; "e" ];
            [ "a"; "f" ];
            [ "f"; "m" ];
            [ "g"; "n" ];
            [ "h"; "n" ];
            [ "i"; "o" ];
            [ "j"; "o" ];
          ]
        |> Datalog.Database.select pattern
  | "flat" ->
      fun pattern ->
        Lwt_seq.of_list
          [ [ "g"; "f" ]; [ "m"; "n" ]; [ "m"; "o" ]; [ "p"; "m" ] ]
        |> Datalog.Database.select pattern
  | "down" ->
      fun pattern ->
        Lwt_seq.of_list
          [
            [ "l"; "f" ];
            [ "m"; "f" ];
            [ "g"; "b" ];
            [ "h"; "c" ];
            [ "i"; "d" ];
            [ "p"; "k" ];
          ]
        |> Datalog.Database.select pattern
  | predicate -> Datalog.Database.empty predicate

let rsg_query_test_cases =
  [
    make_test_case ~name:"Compute entire solution of Reverse-Same-Generation."
      ~database:rsg_database rsg_program "rsg(?x,?y)"
      ~expect:
        [
          (* level 1 *)
          [ "g"; "f" ];
          [ "m"; "n" ];
          [ "m"; "o" ];
          [ "p"; "m" ];
          (* level 2 *)
          [ "a"; "b" ];
          [ "h"; "f" ];
          [ "i"; "f" ];
          [ "j"; "f" ];
          [ "f"; "k" ];
          (* level 3 *)
          [ "a"; "c" ];
          [ "a"; "d" ];
        ];
  ]

let main =
  Logs.set_reporter (Logs_fmt.reporter ());
  Logs.set_level ~all:true (Some Logs.Debug);
  Datalog.set_debug true;
  Alcotest_lwt.run "Datalogl" [ ("rsg", rsg_query_test_cases) ]

let () = Lwt_main.run main
