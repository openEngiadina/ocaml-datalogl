(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax

(* Instantiate Datalog *)
module Datalog = Datalogl.Make (struct
  type t = int

  let compare = Int.compare

  let parser =
    Angstrom.(
      take_while1 (fun c ->
          let code = Char.code c in
          48 <= code && code <= 57)
      >>| int_of_string)

  let pp = Fmt.int
end)

let tuple_set_testable =
  Alcotest.testable Datalog.Tuple.Set.pp (fun a b ->
      Datalog.Tuple.Set.compare a b = 0)

let parse parser string =
  match Angstrom.parse_string ~consume:Angstrom.Consume.All parser string with
  | Ok v -> v
  | Error e -> failwith e

let make_test_case ~name ?expect idb query =
  Alcotest_lwt.test_case name `Quick (fun _switch () ->
      let program = parse Datalog.Program.parser idb in
      let query = parse Datalog.Atom.parser query in
      let* solution =
        Datalog.query ~database:Datalog.Database.empty ~program query
      in
      match expect with
      | Some expect ->
          Alcotest.check tuple_set_testable
            "solution of Datalog evaluation matches expectation"
            (Datalog.Tuple.Set.of_list expect)
            solution;
          return_unit
      | None -> return_unit)

(* Fact queries *)

let facts_query_test_cases =
  [
    make_test_case ~name:"Querying a single fact returns the fact." "f(1)."
      "f(?x)" ~expect:[ [ 1 ] ];
    make_test_case
      ~name:"Querying a single fact with a grounded query returns the fact."
      "f(1)." "f(1)" ~expect:[ [ 1 ] ];
    make_test_case
      ~name:
        "Querying with a grounded fact that is not in the IDB returns an empty \
         soluiton."
      "f(1)." "f(2)" ~expect:[];
    make_test_case ~name:"Irrelevant facts are not in solution"
      "f(1). f(2). f(3). g(2)." "f(1)" ~expect:[ [ 1 ] ];
    make_test_case ~name:"Solution contains all valid facts."
      "f(1). f(2). f(3). g(5)." "f(?x)" ~expect:[ [ 1 ]; [ 2 ]; [ 3 ] ];
    make_test_case ~name:"Querying a single 2-ary fact returns the fact."
      "g(1,2)." "g(?a,?b)"
      ~expect:[ [ 1; 2 ] ];
    make_test_case
      ~name:"Querying for a 2-ary fact with a ground query returns the fact."
      "g(1,2). g(1,3). g(2,2)." "g(1,2)"
      ~expect:[ [ 1; 2 ] ];
    make_test_case
      ~name:
        "Querying for a 2-ary fact with a query where one term is fixed with a \
         constant returns the matching facts."
      "g(1,2). g(1,3). g(2,2)." "g(1,?x)"
      ~expect:[ [ 1; 2 ]; [ 1; 3 ] ];
    make_test_case
      ~name:
        "Querying for a 2-ary fact with same variable in both positions \
         (repeated variable) returns only the matching facts."
      "g(1,2). g(2,2)." "g(?x,?x)"
      ~expect:[ [ 2; 2 ] ];
  ]

let indirect_facts_test_cases =
  [
    make_test_case
      ~name:"Solution contains facts that are inferred from other facts."
      "e(1). p(2). p(3). p(4). e(?x) :- p(?x)." "e(?x)"
      ~expect:[ [ 1 ]; [ 2 ]; [ 3 ]; [ 4 ] ];
    make_test_case
      ~name:
        "Find solutions of indirect facts with repeated variables in the query."
      "p(1,2). p(2,2). e(?a, ?a) :- p(?a, 2)." "e(?x,?y)"
      ~expect:[ [ 1; 1 ]; [ 2; 2 ] ];
  ]

(* Graph queries *)

let graph =
  {graph|
   e(0,1).
   e(1,2).
   e(1,3).
   e(2,4).
   e(3,4).
   
   p(?x,?y) :- e(?x,?y).
   p(?x,?y) :- e(?x,?z), p(?z,?y).
|graph}

let graph_query_test_cases =
  [
    make_test_case ~name:"Edge is a path" graph "p(0,1)" ~expect:[ [ 0; 1 ] ];
    make_test_case ~name:"Path over multiple edges is found" graph "p(0,4)"
      ~expect:[ [ 0; 4 ] ];
    make_test_case ~name:"Can find entire transitive closure" graph "p(?x,?y)"
      ~expect:
        [
          (* reachable from 0 *)
          [ 0; 1 ];
          [ 0; 2 ];
          [ 0; 3 ];
          [ 0; 4 ];
          (* reachable from 1 *)
          [ 1; 2 ];
          [ 1; 3 ];
          [ 1; 4 ];
          (* reachable from 2 *)
          [ 2; 4 ];
          (* reachable from 3 *)
          [ 3; 4 ];
        ];
  ]

(* Reverse Same Generation (RSG)

   This is the example given in "Foundations of Databases" by Serge Abiteboul.
*)

(* We use simple strings as constants. *)
module SymbolConstant = struct
  type t = string

  let compare = String.compare

  let parser =
    Angstrom.(
      take_while (fun c ->
          let code = Char.code c in
          (* ASCII upper case *)
          (65 <= code && code <= 90)
          (* ASCII lower case *)
          || (97 <= code && code <= 122)
          || (* ASCII numbers  *)
          (48 <= code && code <= 57)))

  let pp = Fmt.string
end

let main =
  Logs.set_reporter (Logs_fmt.reporter ());
  Logs.set_level ~all:true (Some Logs.Debug);
  Datalog.set_debug true;
  Alcotest_lwt.run "Datalogl"
    [
      ("facts", facts_query_test_cases);
      ("indirect_facts", indirect_facts_test_cases);
      ("graph", graph_query_test_cases);
    ]

let () = Lwt_main.run main
