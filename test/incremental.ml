(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax

(* Instantiate Datalog *)
module Datalog = Datalogl.Make (struct
  type t = int

  let compare = Int.compare

  let parser =
    Angstrom.(
      take_while1 (fun c ->
          let code = Char.code c in
          48 <= code && code <= 57)
      >>| int_of_string)

  let pp = Fmt.int
end)

let tuple_set_testable =
  Alcotest.testable Datalog.Tuple.Set.pp (fun a b ->
      Datalog.Tuple.Set.compare a b = 0)

let parse parser string =
  match Angstrom.parse_string ~consume:Angstrom.Consume.All parser string with
  | Ok v -> v
  | Error e -> failwith e

let program =
  {graph|
   p(?x,?y) :- e(?x,?y).
   p(?x,?y) :- e(?x,?z), p(?z,?y).
   |graph}
  |> parse Datalog.Program.parser

let database edges = function
  | "e" ->
      fun pattern -> edges |> Lwt_seq.of_list |> Datalog.Database.select pattern
  | predicate -> Datalog.Database.empty predicate

let main =
  (* setup logging *)
  Logs.set_reporter (Logs_fmt.reporter ());
  Logs.set_level ~all:true (Some Logs.Debug);

  (* init state *)
  let state = Datalog.init program in

  (* initial queries with partitioned graph *)
  let edges = [ [ 1; 2 ]; [ 2; 3 ]; [ 4; 5 ] ] in
  let* state, tuples =
    Datalog.(
      query_with_state ~database:(database edges) ~state
      @@ Atom.make "p" Term.[ make_variable "a"; make_variable "b" ])
  in
  Alcotest.check tuple_set_testable "initial query returns partitioned graph"
    (Datalog.Tuple.Set.of_list [ [ 1; 2 ]; [ 1; 3 ]; [ 2; 3 ]; [ 4; 5 ] ])
    tuples;

  (* connect graph *)
  let edges = [ [ 1; 2 ]; [ 2; 3 ]; [ 3; 4 ]; [ 4; 5 ] ] in
  let* state, tuples =
    Datalog.(
      query_with_state ~database:(database edges) ~state
      @@ Atom.make "p" Term.[ make_variable "a"; make_variable "b" ])
  in
  Alcotest.check tuple_set_testable "second query returns connected graph"
    (Datalog.Tuple.Set.of_list
       [
         [ 1; 2 ];
         [ 1; 3 ];
         [ 1; 4 ];
         [ 1; 5 ];
         [ 2; 3 ];
         [ 2; 4 ];
         [ 2; 5 ];
         [ 3; 4 ];
         [ 3; 5 ];
         [ 4; 5 ];
       ])
    tuples;

  (* requery *)
  let edges = [ [ 1; 2 ]; [ 2; 3 ]; (* [ 3; 4 ]; *) [ 4; 5 ] ] in
  let* _state, tuples =
    Datalog.(
      query_with_state ~database:(database edges) ~state
      @@ Atom.make "p" Term.[ make_variable "a"; make_variable "b" ])
  in
  Alcotest.check tuple_set_testable "query result is monotonically increasing"
    (Datalog.Tuple.Set.of_list
       [
         [ 1; 2 ];
         [ 1; 3 ];
         [ 1; 4 ];
         [ 1; 5 ];
         [ 2; 3 ];
         [ 2; 4 ];
         [ 2; 5 ];
         [ 3; 4 ];
         [ 3; 5 ];
         [ 4; 5 ];
       ])
    tuples;

  return_unit

let () = Lwt_main.run main
