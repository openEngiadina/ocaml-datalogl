(use-modules
 (guix packages)
 (guix download)
 (guix git-download)
 (guix build-system dune)
 (guix build-system ocaml)
 (guix build-system gnu)
 ((guix licenses) #:prefix license:)
 (gnu packages autotools)
 (gnu packages maths)
 (gnu packages multiprecision)
 (gnu packages ocaml)
 (gnu packages pkg-config)
 (gnu packages rdf))

(define-public ocaml-base32
  (package
    (name "ocaml-base32")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://inqlab.net/git/ocaml-base32.git")
                     (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ccalgcnx178dmnb3523gv47xf0hbfry45pg7dix64bn86niq4b1"))))
    (build-system dune-build-system)
    (native-inputs
     (list ocaml-alcotest ocaml-qcheck))
    (home-page "https://inqlab.net/git/ocaml-base32.git")
    (synopsis "Base32 encoding for OCaml")
    (description "Base32 is a binary-to-text encoding that represents
binary data in an ASCII string format by translating it into a
radix-32 representation.  It is specified in RFC 4648.")
    (license license:isc)))

(define-public ocaml-cborl
  (package
    (name "ocaml-cborl")
    (version "0.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://inqlab.net/git/ocaml-cborl.git")
                     (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "15bw3s82qpwxs9q42sx9rm0qjk1kdy3zm8m283rc907mls0rjx1i"))))
    (build-system dune-build-system)
    (arguments '())
    (propagated-inputs
     (list ocaml-zarith gmp ocaml-fmt))
    (native-inputs
     (list ocaml-alcotest ocaml-qcheck))
    (home-page "https://inqlab.net/git/ocaml-cborl")
    (synopsis "OCaml CBOR library")
    (description 
     "The Concise Binary Object Representation (CBOR), as specified by
RFC 8949, is a binary data serialization format.  CBOR is similar to
JSON but serializes to binary which is smaller and faster to generate
and parse.  This package provides an OCaml implementation of CBOR.")
    (license license:agpl3+)))

(define-public ocaml-rdf
  (package
    (name "ocaml-rdf")
    (version "76bcc116e9eb0bc42c2743f4a57335a803b03fd3")
    (home-page "https://codeberg.org/openEngiadina/ocaml-rdf.git")
    (source
     (origin
      (method git-fetch)
      (uri (git-reference
	    (url home-page)
            (commit version)))
      (file-name (git-file-name name version))
      (sha256
       (base32 "0y7hn4m0a0v4knygrdan6s4fcjzm6ifq7bywp2ji5kgxbkhaamp8"))))
    (build-system dune-build-system)
    (arguments `(#:tests? #f))
    (native-inputs
     (list ocaml-alcotest
	   ocaml-qcheck))
    (propagated-inputs
     (list
      ;; core dependencies
      ocaml-uri
      ocaml-fmt
      ocaml-uuidm

      ;; serializations
      ocaml-yojson
      ocaml-xmlm
      ocaml-uunf
      ocaml-sedlex

      ;; CBOR
      ocaml-cborl
      ocaml-base64
      ocaml-base32
      ocaml-uri
      ocaml-z3 z3))
    (synopsis "RDF library for OCaml")
    (description #f)
    (license license:agpl3+)))

(define-public ocaml-alcotest-lwt
  (package
    (inherit ocaml-alcotest)
    (name "ocaml-alcotest-lwt")
    (arguments
     `(#:package "alcotest-lwt"
       #:tests? #f))
    (propagated-inputs
     `(("ocaml-alcotest" ,ocaml-alcotest)
       ("ocaml-lwt" ,ocaml-lwt)
       ("ocaml-logs" ,ocaml-logs)))))

(define-public ocaml-ppx-deriving
  (package
    (name "ocaml-ppx-deriving")
    (version "5.2.1")
    (home-page "https://github.com/ocaml-ppx/ppx_deriving")
    (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
          (url home-page)
          (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32
            "1wqcnw4wi6pfjjhixpakckm03dpj990259za432804471a6spm2j"))))
    (build-system dune-build-system)
    (arguments
     `(#:test-target "."))
    (propagated-inputs
      `(("ocaml-ppx-derivers" ,ocaml-ppx-derivers)
        ("ocaml-ppxlib" ,ocaml-ppxlib)
        ("ocaml-result" ,ocaml-result)))
    (native-inputs
      `(("ocaml-cppo" ,ocaml-cppo)
        ("ocaml-ounit2" ,ocaml-ounit2)))
    (properties `((upstream-name . "ppx_deriving")))
    (synopsis
      "Type-driven code generation for OCaml")
    (description
      "ppx_deriving provides common infrastructure for generating
code based on type definitions, and a set of useful plugins
for common tasks. ")
    (license license:expat)))

(define-public ocaml-bheap
  (package
    (name "ocaml-bheap")
    (version "2.0.0")
    (home-page "https://github.com/backtracking/bheap")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32
            "0b8md5zl4yz7j62jz0bf7lwyl0pyqkxqx36ghkgkbkxb4zzggfj1"))))
    (build-system dune-build-system)
    (arguments `(#:test-target "."))
    (native-inputs
      `(("ocaml-stdlib-shims" ,ocaml-stdlib-shims)))
    (synopsis "Priority queues")
    (description
      "Traditional implementation using a binary heap encoded in a resizable array.")
    (license license:lgpl2.1)))

(define-public ocaml-ocamlgraph
  (package
    (name "ocaml-ocamlgraph")
    (version "2.0.0")
    (home-page "https://github.com/backtracking/ocamlgraph/")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32
            "1gjrsyyamvvn2rd9n9yjx6hsglhw0dbm4cgazq0dpx0bbr4inwc3"))))
    (build-system dune-build-system)
    (arguments
     `(#:package "ocamlgraph"
       #:test-target "."))
    (propagated-inputs
     `(("ocaml-stdlib-shims" ,ocaml-stdlib-shims)
       ("ocaml-graphics" ,ocaml-graphics)))
    (synopsis "A generic graph library for OCaml")
    (description
      "Provides both graph data structures and graph algorithms")
    (license license:lgpl2.1)))

(define-public ocaml-uucd
  (package
    (name "ocaml-uucd")
    (version "13.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://erratique.ch/software/uucd/releases/uucd-13.0.0.tbz")
        (sha256
          (base32
            "1fg77hg4ibidkv1x8hhzl8z3rzmyymn8m4i35jrdibb8adigi8v2"))))
    (build-system ocaml-build-system)
    (arguments
     `(#:tests? #f                      ; no tests
       #:build-flags (list "build")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))))
    (propagated-inputs `(("ocaml-xmlm" ,ocaml-xmlm)))
    (native-inputs
     `(("opam" ,opam)
       ("ocamlbuild" ,ocamlbuild)
       ("ocaml-topkg" ,ocaml-topkg)))
    (home-page "https://erratique.ch/software/uucd")
    (synopsis
      "Unicode character database decoder for OCaml")
    (description
      "Uucd is an OCaml module to decode the data of the Unicode character
database from its XML.  It provides high-level (but not necessarily efficient)
access to the data so that efficient representations can be extracted.")
    (license license:isc)))

(define-public ocaml-uucp
  (package
    (name "ocaml-uucp")
    (version "13.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://erratique.ch/software/uucp/releases/uucp-13.0.0.tbz")
        (sha256
          (base32
            "19kf8ypxaakacgg1dwwfzkc2zicaj88cmw11fw2z7zl24dn4gyiq"))))
    (build-system ocaml-build-system)
    (arguments
     `(#:tests? #f                      ; no tests
       #:build-flags (list "build")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))))
    (native-inputs
     `(("ocamlbuild" ,ocamlbuild)
       ("ocaml-topkg" ,ocaml-topkg)
       ("opam" ,opam)
       ("ocaml-uucd" ,ocaml-uucd)
       ("ocaml-uunf" ,ocaml-uunf)
       ("ocaml-uutf" ,ocaml-uutf)))
    (home-page "https://erratique.ch/software/uucp")
    (synopsis "Unicode character properties for OCaml")
    (description "Uucp is an OCaml library providing efficient access to a
selection of character properties of the Unicode character database.  Uucp is
independent from any Unicode text data structure and has no dependencies. It is
distributed under the ISC license.") 
    (license license:isc)))

(define-public ocaml-calendar
  (package
    (name "ocaml-calendar")
    (version "a447a88ae3c1e9873e32d2a95d3d3e7c5ed4a7da")
    (home-page "https://github.com/ocaml-community/calendar/")
    (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
          (url home-page)
          (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32
            "09d9gyqm3zkf3z2m9fx87clqihx6brf8rnzm4yq7c8kf1p572hmc"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'allow-camlfind-destdir
           (lambda _
             (substitute* "Makefile"
               (("\\$\\(CAMLFIND\\) install") "mkdir -p $(DESTDIR) && $(CAMLFIND) install -destdir $(DESTDIR)"))
             #t)))
       #:make-flags
       (list (string-append "DESTDIR=" (assoc-ref %outputs "out")
                            "lib/ocaml/site-lib/"))))
    (native-inputs
     `(("ocaml-findlib" ,ocaml-findlib)
       ("ocaml" ,ocaml)
       ("autoconf" ,autoconf)
       ("automake" ,automake)))
    (synopsis "Library for handling dates and times in your program")
    (description #f)
    (license license:lgpl2.1)))

;; Following two packages are required to run tests for ocaml-eqaf.
;; ocaml-afl-persistent has no build-system and I don't know how to install it...

;; (define-public ocaml-afl-persistent
;;   (package
;;     (name "ocaml-afl-persistent")
;;     (version "1.3")
;;     (home-page "https://github.com/stedolan/ocaml-afl-persistent")
;;     (source
;;       (origin
;;         (method git-fetch)
;;         (uri (git-reference
;;               (url home-page)
;;               (commit (string-append "v" version))))
;;         (file-name (git-file-name name version))
;;         (sha256
;;           (base32
;;             "06yyds2vcwlfr2nd3gvyrazlijjcrd1abnvkfpkaadgwdw3qam1i"))))
;;     (build-system gnu-build-system)
;;     (native-inputs
;;      `(("ocaml", ocaml)
;;        ("autoconf" ,autoconf)
;;        ("automake" ,automake)))
;;     (synopsis "Use afl-fuzz in persistent mode")
;;     (description
;;       "afl-fuzz normally works by repeatedly fork()ing the program being
;; tested. using this package, you can run afl-fuzz in 'persistent mode',
;; which avoids repeated forking and is much faster.")
;; (license license:expat)))

;; (define-public ocaml-crowbar
;;   (package
;;     (name "ocaml-crowbar")
;;     (version "0.2")
;;     (source
;;       (origin
;;         (method url-fetch)
;;         (uri "https://github.com/stedolan/crowbar/archive/v0.2.tar.gz")
;;         (sha256
;;           (base32
;;             "02arkqv0xzmxmpcdmmki2r2bpdk3kzrgllnm36pmr8dw6gw52pjl"))))
;;     (build-system dune-build-system)
;;     (propagated-inputs
;;       `(("ocaml-ocplib-endian" ,ocaml-ocplib-endian)
;;         ("ocaml-cmdliner" ,ocaml-cmdliner)
;;         ("ocaml-afl-persistent" ,ocaml-afl-persistent)))
;;     (native-inputs
;;       `(("ocaml-calendar" ,ocaml-calendar)
;;         ("ocaml-fpath" ,ocaml-fpath)
;;         ("ocaml-uucp" ,ocaml-uucp)
;;         ("ocaml-uunf" ,ocaml-uunf)
;;         ("ocaml-uutf" ,ocaml-uutf)))
;;     (home-page "https://github.com/stedolan/crowbar")
;;     (synopsis
;;       "Write tests, let a fuzzer find failing cases")
;;     (description
;;       "Crowbar is a library for testing code, combining QuickCheck-style
;; property-based testing and the magical bug-finding powers of
;; [afl-fuzz](http://lcamtuf.coredump.cx/afl/).
;; ")
;;     (license #f)))

(define-public ocaml-eqaf
  (package
    (name "ocaml-eqaf")
    (version "0.7")
    (home-page "https://github.com/mirage/eqaf")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))
             ))
       (sha256
        (base32
         "06hsnnjax1kb3qsi3cj0nyyz8c2hj2gbw3h517gpjinpnwy2fr85"))))
    (build-system dune-build-system)
    ;; dependencies for test are hard to build (afl-persistent)
    (arguments `(#:tests? #f))
    (synopsis
     "Constant-time equal function on string")
    (description "This package provides an equal function on string in
constant-time to avoid timing-attack with crypto stuff.")
    (license license:expat)))

(define-public ocaml-digestif
  (package
    (name "ocaml-digestif")
    (version "1.0.0")
    (home-page "https://github.com/mirage/digestif")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (commit (string-append "v" version))))
        (sha256
          (base32
            "0x046by4myiksch16vyhj5l7xkflwhhxm8gzlf7474y0mw77w6lw"))))
    (build-system dune-build-system)
    (propagated-inputs
      `(("ocaml-eqaf" ,ocaml-eqaf)
        ("ocaml-bigarray-compat" ,ocaml-bigarray-compat)
        ("ocaml-stdlib-shims" ,ocaml-stdlib-shims)))
    (native-inputs
      `(("pkg-config" ,pkg-config)
        ("ocaml-fmt" ,ocaml-fmt)
        ("ocaml-alcotest" ,ocaml-alcotest)
        ("ocaml-bos" ,ocaml-bos)
        ("ocaml-astring" ,ocaml-astring)
        ("ocaml-fpath" ,ocaml-fpath)
        ("ocaml-rresult" ,ocaml-rresult)
        ("ocaml-findlib" ,ocaml-findlib)))
    (synopsis "Hashes implementations (SHA*, RIPEMD160, BLAKE2* and MD5)")
    (description
      "Digestif is a toolbox to provide hashes implementations in C and OCaml.

It uses the linking trick and user can decide at the end to use the C implementation or the OCaml implementation.

We provides implementation of:
 * MD5
 * SHA1
 * SHA224\n * SHA256
 * SHA384
 * SHA512
 * BLAKE2B
 * BLAKE2S
 * RIPEMD160
")
    (license license:expat)))

(define-public ocaml-either
  (package
    (name "ocaml-either")
    (version "1.0.0")
    (home-page "https://github.com/mirage/either")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (commit version)))
        (sha256
          (base32
            "099p1m24vz5i0043zcfp88krzjsa2qbrphrm4bnx84gif5vgkxwm"))))
    (build-system dune-build-system)
    (arguments
     `(#:test-target "."))
    (synopsis "Compatibility Either module")
    (description "Projects that want to use the Either module defined in OCaml
4.12.0 while staying compatible with older versions of OCaml should use this
library instead.")
    (license license:expat)))

(define-public ocaml-repr
  (package
    (name "ocaml-repr")
    (version "0.3.0")
    (home-page "https://github.com/mirage/repr")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (commit version)))
        (sha256
          (base32
            "1pim6cpkn71x35k2nkdf5hbv1kinnxkpxi6ff7wwbvsh6kaxrq1g"))))
    (build-system dune-build-system)
    (arguments `(#:package "repr"))
    (propagated-inputs
      `(("ocaml-fmt" ,ocaml-fmt)
        ("ocaml-uutf" ,ocaml-uutf)
        ("ocaml-either" ,ocaml-either)
        ("ocaml-jsonm" ,ocaml-jsonm)
        ("ocaml-base64" ,ocaml-base64)
        ("ocaml-odoc" ,ocaml-odoc)))
    (synopsis
      "Dynamic type representations.  Provides no stability guarantee")
    (description
      "This package defines a library of combinators for building dynamic type
representations and a set of generic operations over representable types, used
in the implementation of Irmin and related packages.  It is not yet intended for
public consumption and provides no stability guarantee.")
    (license license:isc)))

(define-public ocaml-ppx-repr
  (package
    (inherit ocaml-repr)
    (name "ocaml-ppx-repr")
    (arguments `(#:package "ppx_repr"))
    (propagated-inputs
      `(("ocaml-repr" ,ocaml-repr)
        ("ocaml-ppxlib" ,ocaml-ppxlib)
        ("ocaml-ppx-deriving" ,ocaml-ppx-deriving)
        ("ocaml-odoc" ,ocaml-odoc)))
    (native-inputs
      `(("ocaml-hex" ,ocaml-hex)
        ("ocaml-alcotest" ,ocaml-alcotest)))
    (properties `((upstream-name . "ppx_repr")))
    (synopsis "PPX deriver for type representations")
    (description "PPX deriver for type representations")
    (license license:isc)))

(define-public ocaml-ppx-irmin
  (package
    (name "ocaml-ppx-irmin")
    (version "2.6.1")
    (home-page "https://github.com/mirage/irmin")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit version)))
       (sha256
        (base32 "09q02yknb4w6rflhh4iwnz4hcmzapax6mn64phnfa2aab54v5h55"))))
    (build-system dune-build-system)
    (arguments `(#:package "ppx_irmin"))
    (propagated-inputs
     `(("ocaml-ppx-repr" ,ocaml-ppx-repr)))
    (synopsis "PPX deriver for Irmin type representations")
    (description "Irmin is a library for persistent stores with built-in
snapshot, branching and reverting mechanisms. It is designed to use a large
variety of backends. Irmin is written in pure OCaml and does not depend on
external C stubs; it aims to run everywhere, from Linux, to browsers and Xen
unikernels.")
    (license license:isc)))
    
(define-public ocaml-irmin
  (package
    (inherit ocaml-ppx-irmin)
    (name "ocaml-irmin")
    (arguments `(#:package "irmin"))
    (propagated-inputs
      `(("ocaml-repr" ,ocaml-repr)
        ("ocaml-fmt" ,ocaml-fmt)
        ("ocaml-uri" ,ocaml-uri)
        ("ocaml-uutf" ,ocaml-uutf)
        ("ocaml-jsonm" ,ocaml-jsonm)
        ("ocaml-lwt" ,ocaml-lwt)
        ("ocaml-ppx-irmin" ,ocaml-ppx-irmin)
        ("ocaml-digestif" ,ocaml-digestif)
        ("ocamlgraph" ,ocaml-ocamlgraph)
        ("ocaml-logs" ,ocaml-logs)
        ("ocaml-bheap" ,ocaml-bheap)
        ("ocaml-astring" ,ocaml-astring)))
    (native-inputs
      `(("ocaml-hex" ,ocaml-hex)
        ("ocaml-alcotest" ,ocaml-alcotest)
        ("ocaml-alcotest-lwt" ,ocaml-alcotest-lwt)))
    (synopsis "Irmin, a distributed database that follows the same design
principles as Git")))

(define-public ocaml-datalogl
  (package
    (name "ocaml-datalogl")
    (version "0.1.0")
    (source #f)
    (build-system dune-build-system)
    (arguments '())
    (propagated-inputs
     (list ocaml-fmt
	   ocaml-logs
	   ocaml-lwt
	   ocaml-angstrom))
    (native-inputs
     (list ocaml-alcotest ocaml-alcotest-lwt ocaml-rdf))
    (home-page "https://codeberg.org/openEngiadina/ocaml-datalogl")
    (synopsis "A Datalog library for OCaml")
    (description #f)
    (license license:agpl3+)))

ocaml-datalogl
