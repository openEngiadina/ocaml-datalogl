# This Makefile is only for publishing documentation. Use dune to
# build the OCaml library.

.PHONY:
build:
	@echo "Use dune to build."
	@exit 1

.PHONY: doc
doc:
	dune build @doc
	cp -f doc/odoc.css _build/default/_doc/_html/

.PHONY: publish-doc
publish-doc: doc
	rsync -rav -e ssh _build/default/_doc/_html/ qfwfq:/srv/http/inqlab.net-projects/ocaml-datalogl/ --delete
